package com.mandiri.test;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("/test/hello")
    public ResponseEntity<?> getHello(){

        model mod = new model();
        mod.setId("1");
        mod.setName("Daniel Simarmata");
        mod.setCompany("Mandiri");

        return ResponseEntity.ok(mod);


    }
}
